// Created by Ciarán McDermott
// Bluetooth Remote Control App to drive Autonomous Bale Collector



// For performing some operations asynchronously
import 'dart:async';
import 'dart:convert';

//math functions
import 'dart:math';

// For using PlatformException
import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:control_pad/control_pad.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BluetoothApp(),
    );
  }
}

class BluetoothApp extends StatefulWidget {
  @override
  _BluetoothAppState createState() => _BluetoothAppState();
}

class _BluetoothAppState extends State<BluetoothApp> {
  // Initializing the Bluetooth connection state to be unknown
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;
  // Initializing a global key, as it would help us in showing a SnackBar later
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  // Get the instance of the Bluetooth
  FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;
  // Track the Bluetooth connection with the remote device
  BluetoothConnection connection;

  int _deviceState;

  bool isDisconnecting = false;


  double magnitude = 0;
  double direction = 0;

  Map<String, Color> colors = {
    'onBorderColor': Colors.green,
    'offBorderColor': Colors.red,
    'neutralBorderColor': Colors.transparent,
    'onTextColor': Colors.green[700],
    'offTextColor': Colors.red[700],
    'neutralTextColor': Colors.blue,
  };

  // To track whether the device is still connected to Bluetooth
  bool get isConnected => connection != null && connection.isConnected;

  // Define some variables, which will be required later
  List<BluetoothDevice> _devicesList = [];
  BluetoothDevice _device;
  bool _connected = false;
  bool _isButtonUnavailable = false;

  //data to send to robot
  int armUp=0, armDown=0;
  int open=0, close=0;
  double mag=0, dir=0;
  int autonomous=0;

  double drive;
  double turn;

  Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(milliseconds: 100), (Timer t) => sendData());

    // Get current state
    FlutterBluetoothSerial.instance.state.then((state) {
      setState(() {
        _bluetoothState = state;
      });
    });

    _deviceState = 0; // neutral

    // If the bluetooth of the device is not enabled,
    // then request permission to turn on bluetooth
    // as the app starts up
    enableBluetooth();

    // Listen for further state changes
    FlutterBluetoothSerial.instance
        .onStateChanged()
        .listen((BluetoothState state) {
      setState(() {
        _bluetoothState = state;
        if (_bluetoothState == BluetoothState.STATE_OFF) {
          _isButtonUnavailable = true;
        }
        getPairedDevices();
      });
    });
  }

  @override
  void dispose() {
    // Avoid memory leak and disconnect
    if (isConnected) {
      isDisconnecting = true;
      connection.dispose();
      connection = null;
    }
    timer?.cancel();
    super.dispose();
  }

  // Request Bluetooth permission from the user
  Future<void> enableBluetooth() async {
    // Retrieving the current Bluetooth state
    _bluetoothState = await FlutterBluetoothSerial.instance.state;

    // If the bluetooth is off, then turn it on first
    // and then retrieve the devices that are paired.
    if (_bluetoothState == BluetoothState.STATE_OFF) {
      await FlutterBluetoothSerial.instance.requestEnable();
      await getPairedDevices();
      return true;
    } else {
      await getPairedDevices();
    }
    return false;
  }

  // For retrieving and storing the paired devices
  // in a list.
  Future<void> getPairedDevices() async {
    List<BluetoothDevice> devices = [];

    // To get the list of paired devices
    try {
      devices = await _bluetooth.getBondedDevices();
    } on PlatformException {
      print("Error");
    }

    // It is an error to call [setState] unless [mounted] is true.
    if (!mounted) {
      return;
    }

    // Store the [devices] list in the [_devicesList] for accessing
    // the list outside this class
    setState(() {
      _devicesList = devices;
    });
  }

  // Now, its time to build the UI
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text("Bluetooth Remote"),
          backgroundColor: Colors.green,
          shadowColor: Colors.cyan,
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.refresh,
                color: Colors.white,
              ),
              onPressed: () async {
                // So, that when new devices are paired
                // while the app is running, user can refresh
                // the paired devices list.
                await getPairedDevices().then((_) {
                  show('Device list refreshed');
                });
              },
            ),
          ],
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Visibility(
              visible: _isButtonUnavailable &&
                  _bluetoothState == BluetoothState.STATE_ON,
              child: LinearProgressIndicator(
                backgroundColor: Colors.grey,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            ),
            Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Device:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          DropdownButton(
                            items: _getDeviceItems(),
                            onChanged: (value) =>
                                setState(() => _device = value),
                            value: _devicesList.isNotEmpty ? _device : null,
                          ),
                          ElevatedButton(
                            onPressed: _isButtonUnavailable
                                ? null
                                : _connected ? _disconnect : _connect,
                            child:
                            Text(_connected ? 'Disconnect' : 'Connect'),
                          ),
                        ],
                      ),
                    ),
                    const Divider(
                      height: 10,
                      thickness: 1,
                      indent: 0,
                      endIndent: 0,
                      color: Colors.grey,
                    ),/*
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          side: new BorderSide(
                            color: _deviceState == 0
                                ? colors['neutralBorderColor']
                                : _deviceState == 1
                                ? colors['onBorderColor']
                                : colors['offBorderColor'],
                            width: 3,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        elevation: _deviceState == 0 ? 4 : 0,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "DEVICE 1",
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: _deviceState == 0
                                        ? colors['neutralTextColor']
                                        : _deviceState == 1
                                        ? colors['onTextColor']
                                        : colors['offTextColor'],
                                  ),
                                ),
                              ),
                              ElevatedButton(
                                onPressed: _connected
                                    ? _sendOnMessageToBluetooth
                                    : null,
                                child: Text("ON"),
                              ),
                              ElevatedButton(
                                onPressed: _connected
                                    ? _sendOffMessageToBluetooth
                                    : null,
                                child: Text("OFF"),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),*/

                    const SizedBox(height: 30),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column (
                              children: [
                                GestureDetector(
                                  child: Icon(
                                      Icons.arrow_upward_rounded,
                                      color: Colors.grey,
                                      size: 40
                                  ),
                                  onTapDown: (_){
                                    armUp = 1;
                                    //_armUp();
                                    //print("Arm going up");
                                  },
                                  onTapUp: (_){
                                    armUp = 0;
                                    //_armStop();
                                    //print("Arm stopping");
                                  },
                                ),
                                Text("Arm"),
                                GestureDetector(
                                  child: Icon(
                                      Icons.arrow_downward_rounded,
                                      color: Colors.grey,
                                      size: 40
                                  ),
                                  onTapDown: (_){
                                    armDown = 1;
                                    //_armDown();
                                    //print("Arm going down");
                                  },
                                  onTapUp: (_){
                                    armDown = 0;
                                    //_armStop();
                                    //print("Arm stopping");
                                  },
                                ),
                              ]
                          ),
                          Column (
                              children: [
                                GestureDetector(
                                  child: Icon(
                                      Icons.arrow_upward_rounded,
                                      color: Colors.grey,
                                      size: 40
                                  ),
                                  onTapDown: (_){
                                    open = 1;
                                    // _open();
                                    // print("Grabber opening up");
                                  },
                                  onTapUp: (_){
                                    open = 0;
                                    //_grabberStop();
                                    //print("Grabber stopping");
                                  },
                                ),
                                Text("Grabber"),
                                GestureDetector(
                                  child: Icon(
                                      Icons.arrow_downward_rounded,
                                      color: Colors.grey,
                                      size: 40
                                  ),
                                  onTapDown: (_){
                                    close = 1;
                                    //_close();
                                    //print("Grabber closing");
                                  },
                                  onTapUp: (_){
                                    close = 0;
                                    //_grabberStop();
                                    //print("Grabber stopping");
                                  },
                                ),
                              ]
                          ),
                        ]

                    ),

                    const SizedBox(height: 20),
                    JoystickView(
                        size: 350,
                        iconsColor: Colors.white54,
                        backgroundColor: Colors.blueGrey,
                        innerCircleColor: Colors.blueGrey,
                        opacity: 0.5,
                        onDirectionChanged: (double degrees,
                            double distanceFromCenter) {
                          magnitude = distanceFromCenter;
                          direction = degrees;
                          mag = magnitude;
                          dir = direction;
                          //_direction(magnitude, direction);
                          if (degrees > 0 && degrees <= 90) {
                            //print("Top Right");
                          }
                          else if (degrees > 90 && degrees <= 180) {
                            //print("Bottom Right");
                          }
                          else if (degrees > 180 && degrees <= 270) {
                            //print("Bottom Left");
                          }
                          else if (degrees > 270 && degrees <= 360) {
                            //print("Top Left");
                          }
                          //print("degrees = " + direction.toString() + " distance = " + magnitude.toString());
                        },
                        interval: null,
                        showArrows: true
                    ),
                    const SizedBox(height: 10),
                    ElevatedButton(
                      onPressed: () {
                        //autonomous = autonomous==0 ? 1 : 0;
                        if(autonomous==0){
                          autonomousMode();
                        }
                        else {
                          manualMode();
                        }
                      },
                      child:
                      Text(autonomous==1 ? 'Manual' : 'Autonomous'),
                    ),
                  ],
                ),
                Container(
                  color: Colors.blue,
                ),
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Center(),
              ),
            )
          ],
        ),
      ),
    );
  }

  // Create the List of devices to be shown in Dropdown Menu
  List<DropdownMenuItem<BluetoothDevice>> _getDeviceItems() {
    List<DropdownMenuItem<BluetoothDevice>> items = [];
    if (_devicesList.isEmpty) {
      items.add(DropdownMenuItem(
        child: Text('NONE'),
      ));
    } else {
      _devicesList.forEach((device) {
        items.add(DropdownMenuItem(
          child: Text(device.name),
          value: device,
        ));
      });
    }
    return items;
  }

  // Method to connect to bluetooth
  void _connect() async {
    setState(() {
      _isButtonUnavailable = true;
    });
    if (_device == null) {
      show('No device selected');
    } else {
      if (!isConnected) {
        await BluetoothConnection.toAddress(_device.address)
            .then((_connection) {
          print('Connected to the device');
          connection = _connection;
          setState(() {
            _connected = true;
          });

          connection.input.listen(null).onDone(() {
            if (isDisconnecting) {
              print('Disconnecting locally!');
            } else {
              print('Disconnected remotely!');
            }
            if (this.mounted) {
              setState(() {});
            }
          });
        }).catchError((error) {
          print('Cannot connect, exception occurred');
          print(error);
        });
        show('Device connected');

        setState(() => _isButtonUnavailable = false);
      }
    }
  }

  // void _onDataReceived(Uint8List data) {
  //   // Allocate buffer for parsed data
  //   int backspacesCounter = 0;
  //   data.forEach((byte) {
  //     if (byte == 8 || byte == 127) {
  //       backspacesCounter++;
  //     }
  //   });
  //   Uint8List buffer = Uint8List(data.length - backspacesCounter);
  //   int bufferIndex = buffer.length;

  //   // Apply backspace control character
  //   backspacesCounter = 0;
  //   for (int i = data.length - 1; i >= 0; i--) {
  //     if (data[i] == 8 || data[i] == 127) {
  //       backspacesCounter++;
  //     } else {
  //       if (backspacesCounter > 0) {
  //         backspacesCounter--;
  //       } else {
  //         buffer[--bufferIndex] = data[i];
  //       }
  //     }
  //   }
  // }

  // Method to disconnect bluetooth
  void _disconnect() async {
    setState(() {
      _isButtonUnavailable = true;
      _deviceState = 0;
    });


    await connection.close();
    show('Device disconnected');
    if (!connection.isConnected) {
      setState(() {
        _connected = false;
        _isButtonUnavailable = false;
      });
    }
  }

  void autonomousMode() async {
    setState(() {
      autonomous = 1;
    });
  }

  void manualMode() async {
    setState(() {
      autonomous = 0;
    });
  }

  double degreeToRadian(double degree) {
    return degree*pi/180;
  }

  double radianToDegree(double radian) {
    return radian*180/pi;
  }

  void sendData() {

    //find magnitude of forward/backward and turn
    if(dir>=0 && dir <90){
      drive = mag*cos(degreeToRadian(dir));
      turn = mag*sin(degreeToRadian(dir));
      print("Top Right");
    }
    else if(dir>=90 && dir <180){
      drive = mag*cos(degreeToRadian(dir));
      turn = mag*sin(degreeToRadian(dir));
      print("Bottom Right");
    }
    else if(dir>180 && dir<=270){
      drive = mag*cos(degreeToRadian(dir));
      turn = mag*sin(degreeToRadian(dir));
      print("Bottom Left");
    }
    else if(dir>270 && dir<=360){
      drive = mag*cos(degreeToRadian(dir));
      turn = mag*sin(degreeToRadian(dir));
      print("Top Left");
    }
    else {
      drive = 0;
      turn =0;
    }

      //connection.output.add(utf8.encode("\nData: mag="+mag.toString() + " dir="+dir.toString()+ " armUp="+armUp.toString() + " armDown="+armDown.toString() + " open="+open.toString() + " close="+close.toString() + "\r\n"));
      connection.output.add(utf8.encode("\n"+drive.toStringAsFixed(2) + ","+turn.toStringAsFixed(2)+ ","+armUp.toString() + ","+armDown.toString() + ","+open.toString() + ","+close.toString() + "," + autonomous.toString() + "@\r\n"));
      print("Data: mag="+drive.toStringAsFixed(2) + " dir="+turn.toStringAsFixed(2)+ " armUp="+armUp.toString() + " armDown="+armDown.toString() + " open="+open.toString() + " close="+close.toString() + " autonomous="+ autonomous.toString() + "@\r\n");

  }


  void _armUp() async {
    connection.output.add(utf8.encode("ArmUp" + "\r\n"));
    await connection.output.allSent;
  }

  void _armStop() async {
    connection.output.add(utf8.encode("ArmStop" + "\r\n"));
    await connection.output.allSent;
  }

  void _armDown() async {
    connection.output.add(utf8.encode("ArmDown" + "\r\n"));
    await connection.output.allSent;
  }

  void _open() async {
    connection.output.add(utf8.encode("Open" + "\r\n"));
    await connection.output.allSent;
  }

  void _grabberStop() async {
    connection.output.add(utf8.encode("GStop" + "\r\n"));
    await connection.output.allSent;
  }

  void _close() async {
    connection.output.add(utf8.encode("Close" + "\r\n"));
    await connection.output.allSent;
  }

  void _direction(double mag, double dir) async {
    connection.output.add(utf8.encode("mag="+mag.toDouble().toString() + "\t deg=" + dir.toDouble().toString() + "\r\n"));
    await connection.output.allSent;
  }




  // Method to send message,
  // for turning the Bluetooth device on
  void _sendOnMessageToBluetooth() async {
    connection.output.add(utf8.encode("1" + "\r\n"));
    await connection.output.allSent;
    show('Device Turned On');
    setState(() {
      _deviceState = 1; // device on
    });
  }

  // Method to send message,
  // for turning the Bluetooth device off
  void _sendOffMessageToBluetooth() async {
    connection.output.add(utf8.encode("0" + "\r\n"));
    await connection.output.allSent;
    show('Device Turned Off');
    setState(() {
      _deviceState = -1; // device off
    });
  }

  // Method to show a Snackbar,
  // taking message as the text
  Future show(
      String message, {
        Duration duration: const Duration(seconds: 3),
      }) async {
    await new Future.delayed(new Duration(milliseconds: 100));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
    /*_scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        content: new Text(
          message,
        ),
        duration: duration,
      ),
    );*/
  }
}
